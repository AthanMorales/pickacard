/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.sheridancollege.softwarefundamentals.pickacard;
import java.util.Scanner;
/**
 * A class that fills a magic hand of 7 cards with random Card Objects
 * and then asks the user to pick a card and searches the array of cards
 * for the match to the user's card. To be used as starting code in ICE 1
 * @author dancye
 */
public class CardTrick {
    
    public static void main(String[] args)
    {
        Scanner read = new Scanner(System.in);
        Card[] magicHand = new Card[7];
        
        for (int i=0; i<magicHand.length; i++)
        {
            Card c= new Card();//object
           
            //c.setValue(insert call to random number generator here)
            //c.setSuit(Card.SUITS[insert call to random number between 0-3 here])
            c.setValue(c.randomValue());
            c.setSuit(c.randomSuit());
            magicHand[i]=c;//saving object in array
            System.out.println(magicHand[i].getValue()+" "+magicHand[i].getSuit());
        }
        
        //insert code to ask the user for Card value and suit, create their card
        // and search magicHand here
        //Then report the result here
        
        //take input from user and compare with array\
        Card lockycard= new Card();

        //validate number user input
        do{
            System.out.println("Please enter a card number between 1 and 13");
            lockycard.setValue(read.nextInt());
            
            //usernumber = read.nextInt();
            if(lockycard.getValue() < 1 || lockycard.getValue() > 13){
                System.out.println("Number no valid!\t\t Try again");
            }
        }while(lockycard.getValue() < 1 || lockycard.getValue() > 13);
        read.nextLine();//clean buffer
        
        //validate suit user input
        boolean x=false;
        do{
            System.out.println("Please enter you suit1  (Diamonds, Clubs, Spades or Heards)");
            lockycard.setSuit(read.nextLine());
            if(lockycard.getSuit().equals("Diamonds")||lockycard.getSuit().equals("Clubs")||lockycard.getSuit().equals("Spades")||lockycard.getSuit().equals("Heards")){
                x=true;
            }else{
                System.out.println("Suit no valid! Try again");
            }
        }while(!x);
        
        //compare with array
        for(int i=0;i<7;i++){
            if(lockycard.getValue()==magicHand[i].getValue()&& lockycard.getSuit().equals(magicHand[i].getSuit())){
                System.out.println("YEA!!! Your card is in the MagicHand!!!");
                break;
            }else{
                if(i==6){
                    System.out.println("UPS!! Your card is not in the MagicHand :(");
                }
            }
        }   
    }
}
